document.addEventListener('DOMContentLoaded', function () {
    const todoItems = document.getElementById('todo-items');

    // Function to handle editing a todo item
    function handleEdit() {
        const todo = this.parentElement.parentElement;
        const todoText = todo.querySelector('label').textContent;
        const input = document.createElement('input');
        input.type = 'text';
        input.value = todoText;
        todo.querySelector('label').replaceWith(input);
        input.focus();
        input.addEventListener('blur', () => {
            const label = document.createElement('label');
            label.textContent = input.value;
            input.replaceWith(label);
            updateDateLabel(todo); 
        });
    }

    // Function to handle deleting a todo item
    function handleDelete() {
        this.parentElement.parentElement.remove();
    }

    // Function to update the date label
    function updateDateLabel(todo) {
        const dateLabel = todo.querySelector('.date-label');
        const currentDate = new Date();
        const formattedDate = currentDate.toLocaleDateString();
        dateLabel.textContent = `Last edited: ${formattedDate}`;
        dateLabel.style.display = 'inline';
    }

    // Attach event listeners to each edit and delete button
    function attachButtonListeners() {
        const editButtons = document.querySelectorAll('.edit-btn');
        editButtons.forEach(button => {
            button.addEventListener('click', handleEdit);
        });

        const deleteButtons = document.querySelectorAll('.delete-btn');
        deleteButtons.forEach(button => {
            button.addEventListener('click', handleDelete);
        });
    }

    attachButtonListeners(); 

    // Adding a new todo item
    const addTodoBtn = document.querySelector('.add-todo-btn');
    addTodoBtn.addEventListener('click', () => {
        const todoText = document.querySelector('.add-todo').value;
        if (todoText) {
            const todo = document.createElement('li');
            todo.className = 'todo-item';
            todo.innerHTML = `
                    <div>
                        <input type="checkbox" class="checkbox">
                        <label>${todoText}</label>
                    </div>
                    <div class="todo-actions">
                        <button class="edit-btn">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button class="delete-btn">
                            <i class="fa fa-trash-o"></i>
                        </button>
                    </div>
                    <span class="date-label"></span>
                `;
            todoItems.appendChild(todo);
            document.querySelector('.add-todo').value = '';

            attachButtonListeners(); 
            updateDateLabel(todo); 
        }
    });
});